%global nspr_version 4.36

Name:           nspr
Version:        4.36.0
Release:        1
Summary:        Netscape Portable Runtime
License:        MPL-2.0
URL:            https://firefox-source-docs.mozilla.org/nspr/index.html
BuildRequires:  gcc 
Source0:        https://ftp.mozilla.org/pub/nspr/releases/v%{nspr_version}/src/%{name}-%{nspr_version}.tar.gz
Source1:        nspr-config.xml

Patch1:		nspr-gcc-atomics.patch

%description
NetScape Portable Runtime (NSPR) provides platform independence for non-GUI
operating system facilities. These facilities include threads, thread
synchronization, normal file and network I/O, interval timing and calendar
time, basic memory management (malloc and free) and shared library linking.

%package devel
Summary:        Development libraries for the Netscape Portable Runtime
BuildRequires:  xmlto
Requires:       %{name} = %{version}-%{release}

%description devel
Development package for nspr.

%prep
%autosetup -n %{name}-%{nspr_version} -p1
cp ./nspr/config/nspr-config.in ./nspr/config/nspr-config-pc.in

%build
%define _configure ./nspr/configure
%configure \
                 --includedir=%{_includedir}/nspr4 \
                 --enable-64bit \
                 --enable-optimize="$RPM_OPT_FLAGS" \
                 --disable-debug
%make_build
date +"%e %B %Y" | tr -d '\n' > date.xml
echo -n %{version} > version.xml
cp %{SOURCE1} .
xmlto man nspr-config.xml

%check
perl ./nspr/pr/tests/runtests.pl 2>&1 | tee output.log
TEST_FAILURES=`grep -c FAILED ./output.log` || :
if [ $TEST_FAILURES -ne 0 ]; then
  echo "error: test suite returned failure(s)"
  exit 1
fi
echo "test suite completed"

%install
%make_install
%delete_la_and_a

mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/pkgconfig
NSPR_LIBS=`./config/nspr-config --libs`
NSPR_CFLAGS=`./config/nspr-config --cflags`
NSPR_VERSION=`./config/nspr-config --version`
install -c -m 644 nspr-config.1 $RPM_BUILD_ROOT%{_mandir}/man1/nspr-config.1

rm -f %{buildroot}%{_bindir}/compile-et.pl %{buildroot}%{_bindir}/prerr.properties

%files
%{_libdir}/libnspr4.so
%{_libdir}/libplc4.so
%{_libdir}/libplds4.so
%license nspr/LICENSE

%files devel
%{_bindir}/nspr-config
%{_libdir}/pkgconfig/nspr.pc
%{_includedir}/nspr4
%{_datadir}/aclocal/nspr.m4
%{_mandir}/man*/*

%changelog
* Thu Oct 24 2024 Funda Wang <fundawang@yeah.net> - 4.36.0-1
- update to 4.36.0

* Tue Apr 23 2024 Liu Chao <liuchao173@huawei.com> - 4.35.0-3
- update nspr-4.35.tar.gz

* Fri Feb 24 2023 Wenyu Liu <liuwenyu7@huawei.com> - 4.35.0-2
- disable debug option

* Wed Dec 28 2022 Wenyu Liu <liuwenyu7@huawei.com> - 4.35.0-1
- update to 4.35.0

* Mon Nov 14 2022 zhaozhen <zhaozhen@loongson.cn> - 4.32.0-2
- Support loongarch64

* Mon Nov 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 4.32.0-1
- update to 4.32.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 4.29.0-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Mon Feb 1 2021 xinghe <xinghe1@huawei.com> -  4.29.0-1
- update to 4.29.0

* Fri Sep 4 2020 shixuantong <shixuantong@huawei.com> -  4.26.0-3
- update Source0 and change tar package name

* Thu Jul 30 2020 jinzhimin<jinzhimin2@huawei.com> - 4.26.0-2
- fix build failed

* Fri Jul 17 2020 jinzhimin <jinzhimin2@huawei.com> - 4.26.0-1
- upgrade to 4.26.0

* Wed Jan 8 2020 hewenliang <hewenliang4@huawei.com> - 4.20.0-4
- Remove unecessary patches.

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.20.0-3
- Fix ldconfig scriptlets

* Fri Apr 19 2019 liusirui<liusirui@huawei.com> - 4.20.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport patches from https://hg.mozilla.org/projects/nspr

* Mon Sep 10 2018 openEuler Buildteam <buildteam@openeuler.org> - 4.20.0-1
- Package init
